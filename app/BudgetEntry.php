<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetEntry extends Model
{
    protected $table = 'BudgetEntries';
}
