<?php

namespace App\Http\Controllers;

use Request;
use Redirect;
use App\BudgetCategory;
use App\BudgetEntry;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class BudgetCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $categories = BudgetCategory::all();
        return View::make('category.index')
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
            'name'       => 'required',
        );
        $validator = Validator::make(Request::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('categories/create');
        } else {
            // store
            $cat = new BudgetCategory();
            $cat->name       = Request::get('name');
            $cat->save();

            // redirect
            return Redirect::to('categories');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entries = BudgetEntry::where('categoryId', $id)->get();
        return View::make('category.show')->with('entries', $entries);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = BudgetCategory::find($id);

        return View::make('category.edit')
            ->with('category', $cat);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'       => 'required',
        );
        $validator = Validator::make(Request::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('categories/' . $id . '/edit');
        } else {
            // store
            $cat = BudgetCategory::find($id);
            $cat->name       = Request::get('name');
            $cat->save();

            // redirect
            return Redirect::to('categories');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $cat = BudgetCategory::find($id);
        $expenses = BudgetEntry::where('categoryId', $id)->get();
        foreach($expenses as $e){
            $e->delete();
        }
        $cat->delete();
            return Redirect::to('categories');
    }
}
