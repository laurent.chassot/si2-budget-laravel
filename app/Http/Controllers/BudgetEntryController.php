<?php

namespace App\Http\Controllers;

use Request;
use Redirect;
use App\BudgetCategory;
use App\BudgetEntry;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;

class BudgetEntryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $entries = BudgetEntry::all();
        return View::make('entry.index')
            ->with('entries', $entries);
    }

    public function create()
    {
        return View::make('entry.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'description'       => 'required',
            'value'      => 'required',
            'categoryId' => 'required',
            'dates' => 'required',
        );

        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('entries');
        } else {
            // store
            $e = new BudgetEntry();
            $e->description   = Request::get('description');
            $e->value      = Request::get('value');
            $e->categoryId = Request::get('categoryId');
            $e->date = Request::get('dates');
            $e->save();

            // redirect
            return Redirect::to('entries');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $e = BudgetEntry::find($id);

        return View::make('entry.edit')
            ->with('entry', $e);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'description'       => 'required',
            'value'      => 'required',
            'categoryId' => 'required',
            'dates' => 'required',
        );

        $validator = Validator::make(Request::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('entries');
        } else {
            // store
            $e = BudgetEntry::find($id);
            $e->description   = Request::get('description');
            $e->value      = Request::get('value');
            $e->categoryId = Request::get('categoryId');
            $e->date = Request::get('dates');
            $e->save();

            // redirect
            return Redirect::to('entries');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $e = BudgetEntry::find($id);
        $e->delete();
        return Redirect::to('entries');
    }
}
