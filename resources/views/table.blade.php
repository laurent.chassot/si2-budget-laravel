<table class="table" style="margin-top:40px;">
    <thead>
        <tr>
            <th scope="col">Date</th>
            <th scope="col">Description</th>
            <th scope="col">Category</th>
            <th scope="col">Price</th>
            <th scope="col" class="text-right">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($entries as $e)
        <tr>
            <td>{{$e->date}}</td>
            <td>{{$e->description}}</td>
            <td><a href="{{ URL::to('categories/' . $e->categoryId) }}"><?php
                $c = App\BudgetCategory::find($e->categoryId);
                echo $c->name;
                ?></a>
            </td>
            <td>{{$e->value}}</td>
            <td class="text-right">
                <a class="btn btn-small btn-info" href="{{ URL::to('entries/' . $e->id . '/edit') }}">Edit</a>
                {{ Form::open(array('url' => 'entries/' . $e->id, 'style' => 'display:inline;')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

