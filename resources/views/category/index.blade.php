@extends('default')

@section('content')
<div class="row">
    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Budget Planner</h1>
        <p class="lead">The perfect tool for broke students!</p>
    </div>
    <div class="col-lg-12">
        <a href="{{ URL::to('categories/create') }}"><button type="button" class="btn btn-primary">New category</button></a>
        <br>
        <table class="table" style="margin-top:40px;" >
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $e)
                <tr>
                    <td><a href="{{ URL::to('categories/' . $e->id) }}">{{$e->name}}</a></td>
                    <td class="text-right">
                        <a class="btn btn-small btn-info" href="{{ URL::to('categories/' . $e->id . '/edit') }}">Edit</a>
                        {{ Form::open(array('url' => 'categories/' . $e->id, 'style' => 'display:inline;')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
