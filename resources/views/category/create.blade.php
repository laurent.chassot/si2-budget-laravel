@extends('default')

@section('content')
<h1>Create a category</h1>
<form action="/api/categories" method="POST" style="margin-bottom:40px;">
    @csrf

    <div class="form-group text-left">
        <label for="name">Name</label>
        <input required type="text" class="form-control" name="name" id="name" placeholder="Ex. Food">
    </div>

    <button class="btn btn-primary float-right" type="submit">Add category</button>

</form>
@endsection
