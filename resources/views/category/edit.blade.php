@extends('default')

@section('content')
<h1>Edit {{ $category->name }}</h1>

{{ Form::model($category, array('route' => array('categories.update', $category->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@endsection
