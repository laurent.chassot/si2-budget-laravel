@extends('default')


@section('content')
<h1>New Expense</h1>
<form action="/api/entries" method="POST" style="margin-bottom:40px;">
@csrf
    <div class="form-group text-left">
        <label for="description">Description</label>
        <input required type="text" class="form-control" name="description" id="description" placeholder="Ex. Thursday night party">
    </div>
    <div class="form-group text-left">
        <label for="value">Price</label>
        <input required type="number" step="any" class="form-control" name="value" id="value">
    </div>
    <div class="form-group text-left">
        <label for="date">Date</label>
        <input type="text" id="date" name="dates">
    </div>
    <?php
    $categories = App\BudgetCategory::all();
    ?>
    <div class="form-group text-left">
        <label for="cat">Category</label>
        <select required class="form-control" name="categoryId" id="categoryId">

            @foreach($categories as $c)
            <option value="{{$c->id}}">{{$c->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <button class="btn btn-primary float-right" type="submit">Add expense</button>
    </div>

</form>
@endsection
