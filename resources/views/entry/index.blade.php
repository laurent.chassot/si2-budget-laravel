@extends('default')

@section('content')
<div class="row">
    <div class="col-lg-12 text-center">
        <h1 class="mt-5">Budget Planner</h1>
        <p class="lead">The perfect tool for broke students!</p>
    </div>
    <div class="col-lg-12">
        <a href="{{ URL::to('entries/create') }}"><button type="button" class="btn btn-primary">New expense</button></a>
        @component('table', ['entries' => $entries])
        @endcomponent
    </div>
</div>
</div>
@endsection
