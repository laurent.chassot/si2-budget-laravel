@extends('default')

@section('content')
<h1>Edit {{ $entry->description }}</h1>

{{ Form::model($entry, array('route' => array('entries.update', $entry->id), 'method' => 'PUT')) }}

<div class="form-group text-left">
    <label for="description">Description</label>
    <input required type="text" class="form-control" name="description" id="description" placeholder="Ex. Thursday night party" value="{{$entry->description}}">
</div>
<div class="form-group text-left">
    <label for="value">Price</label>
    <input required type="number" step="any" class="form-control" name="value" id="value" value="{{$entry->value}}">
</div>
<div class="form-group text-left">
    <label for="date">Date</label>
    <input type="text" id="date" name="dates" value="{{$entry->date}}">
</div>
<?php
$categories = App\BudgetCategory::all();
?>
<div class="form-group text-left">
    <label for="cat">Category</label>
    <select required class="form-control" name="categoryId" id="categoryId">

        @foreach($categories as $c)
        <option value="{{$c->id}}" <?php if($c->id == $entry->categoryId) echo "selected"; ?>>{{$c->name}}</option>
        @endforeach
    </select>
</div>

{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@endsection
