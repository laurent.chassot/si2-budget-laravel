@extends('default')

@section('content')
<?php

use App\BudgetEntry;

$cat = App\BudgetCategory::all()->values();
$data = BudgetEntry::groupBy('categoryId')
    ->selectRaw('sum(value) as sum')
    ->pluck('sum')->values();

$categories = $cat->map(function ($e) {
    return $e->name;
})->toArray();
?>

<div class="row">
    <canvas id="myChart" width="400" height="100"></canvas>
    @component('table', ['entries' => $entries])
    @endcomponent
</div>

<script>
    var dynamicColors = function() {
        var r = Math.floor(Math.random() * 255);
        var g = Math.floor(Math.random() * 255);
        var b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    }
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: <?php echo json_encode($categories); ?>,
            datasets: [{
                label: 'Categories',
                data: <?php echo json_encode($data); ?>,
                backgroundColor: [<?php foreach($data as $d) { echo "dynamicColors(),";}?>]
            }]
        }
    });
</script>
@endsection
