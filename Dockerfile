FROM php:7
RUN apt-get update -y && apt-get install -y openssl zlib1g-dev zip unzip git libzip-dev
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo mbstring zip
WORKDIR /app
COPY . /app
RUN composer install
RUN php artisan migrate
CMD php artisan serve --host=0.0.0.0 --port=8181
EXPOSE 8181
